# Business Unit
## Identity
A Business Unit is a piece of the circle Consulting specialized into the delivery of projects for a specific domain/industry.

### Purpose
As a part of Consulting, BUs inherit the motto "People first Delivery".

This means building our business value on our [company values](Values.md).

### Leadership vs Management

A Business Unit requires attention to leadership and management. 

BUs’ accountabilities are related to both those aspects.

[Leadership](LeadershipAndCoaching.md) drives [people growth](PeopleGrowth.md) and continuous improvement.

Management drives administration of a BU, connection with other circles, planning artifacts, and processes.

Of course, leadership and management are strongly intertwined.

A Business Unit is responsible for the professional growth of its members.

### Accountabilities
- Plan and schedule demand, capacity, utilization, and priorities of agilers
- Communicate recruiting needs towards People Hiring and Administration and Engineering Director
- Identify and mitigate project risks and find and promote opportunities with Account and Sales
- Deliver projects in alignment with all Consulting practices and principles
- Provide fair and recognizable career paths in alignment with the career ladder. Adapt compensation to the actual status of agilers' growth
- Ensure timesheet compliance with Finance needs and Consulting metrics
- Monitor customer satisfaction and drive remediation actions
- Plan and execute training activities in alignment with Consulting strategy
- Execute mentorship on Hard and Soft skills, coordinating with Training
- Support Sales in pre-sales activities, including tenders
- Promote the business unit through domain-specific activities with Marketing
- Implement practices in alignment with the purpose

### Demand & Capacity Planning
- The resource demand is reported to an Excel placed in `BigData / Capacity Plan`
- A recurrent meeting between BUs and Engineering Director is used to discuss opportunities and risks
- This meeting triggers recruiting actions and moves among BUs

### Members
An agiler belongs to a Business Unit for a certain quota reported to `BigData / Capacity Plan`.
The availability is established by the Engineering Director.

The capacity plan of Engine determines the availability of BU members.

### Business Units

* [Utility & Telco](bu/utilitytelco/BusinessUnit.md)
* Insurance & Telematics
* [Banking & Payments](BankingAndPayments.md)
    * [Knowledge Challenge Pills](KnowledgeChallengePills.md)
* Services
* Logistics
* Incubation
