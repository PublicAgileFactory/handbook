# Rende

## How to reach us
- Address: [Via Pedro Alvares Cabral, 6, 87036 Rende CS](https://maps.app.goo.gl/6D3NFNMiMMmhUazd8)
    - [Website](https://talentgarden.org/it/coworking/italy/cosenza/)
- Parking: Free
- By bus:
    * Local Bus [link](https://moovitapp.com/cosenza-3280/poi/Talent%20Garden%20Cosenza/Cosenza/it?ref=2&customerId=4908&tll=39.358202_16.246198&fll=39.298257_16.253833&t=1) and then a 10 minute walk

## Office
- Spaces:
  * _Operative area_: 5 desks in an open-space, fully equipped with external screens, keyboards, mouses, wired and wi-fi connection
  * 2 conference room, also usable for private calls and meetings (to be booked in advance - please refer to the Rende {% role %}People Care/Facility{% endrole %} for instructions)
  * 1 event space (to be booked in advance - please refer to the Rende {% role %}People Care/Facility{% endrole %} for instructions)
  * _Recreative area_: coffee zone, ping-pong table
  * _Relax area_ : sofa, treadmill

## Patronal Feast
- The office will be closed on **February 20th**.

## Emergency guidelines
You can find the emergency procedure guidelines at the following [link](https://agilelab.sharepoint.com/:f:/s/bigdata/EggaSJMLNXtLtDAuLLM-E6MBtoeC9v1hxvqnCA8QwKunog?e=v0godN)