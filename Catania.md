# Catania

## Office

- **Address:** [CoWonderful Via Vincenzo Giuffrida, 203/A, 95128 Catania CT](https://goo.gl/maps/bsW5Z6ZHWwFVZZca8)  
  * [Website](https://www.cowonderful.com)

- **Rooms:**  
  * The office is located on the 4th floor; the reception answers the intercom from 9 am to 6 pm.  
  * _Operative area_: 2 seats in an open-space that has a total of 4 desks, equipped with external monitors and Wi-Fi connection.  
  * _Recreational area_: Coffee zone.

- **Parking:**  
  * Limited to 1 spot in the internal parking area (please contact Catania {% role %}People Care/Facility{% endrole %} for further instructions).  
  * Plenty of public parking spaces available on the nearby streets.

## How to reach us

- **By bus:**  
  * Any bus stopping at *Piazzale Sanzio*.  
  * Buses stopping at *Giuffrida Cosenza* or *Giuffrida Pirandello* (e.g., Line BRT5).

- **By metro:**  
  * The closest stations are *Giuffrida* (8-minute walk) and *Borgo Sanzio* (12-minute walk).

## Patronal Feast

- The office will be closed on **February 4th and 5th**.

## Emergency guidelines
You can find the emergency procedure guidelines at the following [link](https://agilelab.sharepoint.com/:f:/s/bigdata/EqDvYBkGEZBFr9R5UuD4LksBm1QrPeDnyEcQVWhGJVAEtQ?e=9xpoiI)
