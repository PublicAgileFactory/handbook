# Workshops

A workshop is a structured session designed to facilitate learning, knowledge sharing, and skill development among employees.

The goal of workshops is:
* to transfer *company knowledge* in order to be aligned on a common technical direction and 
* to share previous experiences.

Workshops can take different formats, including presentations, interactive coding sessions, problem-solving exercises, and live discussions. They may be led by experienced engineers, guest speakers, or subject matter experts who provide insights and guidance on specific topics.


{% role %}Training/Workshop steward{% endrole %} is the reference point to workshop activities whether you ask for or want organize it.

Attending a workshop has to be considered as part of the personal time budget for training (as in [Benefits](Benefits.md)) and hours spent should be logged as `elapseit:<workshop year> Agile - Training - Personal Budget`.

## How to be speaker

If you feel that you master a topic or you want to share a valuable experience or lesson learned with the company you can be an eligible guest speaker: ask {% role %}Training/Workshop steward{% endrole %} to signal your interest, they will provide guidance on preparing the workshop, organizing, and run it in front of an audience.

As reminder, another  reason to propose yourself as speaker, is the `ladder:EngineerIII` requirement which states `Mentors junior engineers via pairing, design review, and code review. Contributes frequently to workshops and knowledge sharing events`. 

If the workshop is about a topic listed in the Training's set of target topics for the current year (you can find it in the [Training Portal](https://agilefactory.gitlab.io/training/training-portal/docs/tech-strategy)), then the creation is confirmed to be part of training activities (and budget). Otherwise, its feasibility should be validated with the `circle:Training/Workshop steward` (or a better fitting role, with specific accountability, if existing).

Log the time spent on `elapseit:<workshop year> Agile - Training - Course or Workshop Preparation`. 

## How to request a workshop

If you feel that a topic should be covered by a workshop please contact the {% role %}Training/Workshop steward{% endrole %}, they will collect your feedback. You could also post a request in the Teams channel Training>Workshops.

## How to organize a workshop

All the heavy lifting about the organization may be performed by the {% role %}Training/Workshop steward{% endrole %}.
You only need to provide the following information:
* Title of the workshop.
* Abstract of the workshop.

The {% role %}Training/Workshop steward{% endrole %} will help you to get everthing that is needed to set up the workshop.



Log the time spent on `elapseit:<workshop year> Agile - Training - Course or Workshop Preparation`. 

**IMPORTANT NOTE**: The workshop material (slides, videos, etc ...) could be a valuable asset to be reused for trainings. It can be the uselful to bring such material in the form of a learning module on the company's LMS. Scale economy is our friend!

## Deliverables and accountabilities

It is duty of {% role %}Training/Workshop steward{% endrole %} to:

* Record the Microsoft Teams meeting.
* Post the session recording on Microsoft Streams, include relevant links to content directly in the Streams recording description.
* Share slides in a sharing point folder.
* If the workshop has code examples, provide a gitlab repository.
* Publish the workshop information and relevant links to the [Training Portal](https://agilefactory.gitlab.io/training/training-portal/).
* If valuable, coordinate with {% role %}Training/LMS Course steward{% endrole %} to publish workshop as a course on LMS.
* Collect the attendance report of the Teams Meeting.
* Ensure that attendees are aware that hours spent attending the workshop should be logged as `elapseit:<workshop year> Agile - Training - Personal Budget` and it has to be considered as part of the personal time budget for training (as in [Benefits](Benefits.md)).
* Collect and record attendees feedback in `sharepoint://BigData/Documents/training/workshops/`.
