In case of crisis, if the continuity of our business is in the danger we rely on the Business Continuity Officier ( BCO ) role.
This role has the duty to engage and compose the Crisis Management Committee ( CMC ).

The CMC is the team that will pro-actively manage the crisis and will take all the major decisions and will supervise all the activities and involved resources. It has the accountability to activate and execute the BC and DR plan.
All the decisions of the committee will be documented when the crisis will finish and this must be signed by the BCO.
This document should include all the information related to the activation of the process, and how the crisis has been closed.

The CMC is accountable for:
- Define, approve and update the BC plan
- Evaluate emergency situations and declare the crisis event
- Activate all the recover activities
- External and internal communications
- Control and monitoring of operations
- Declare the crisis end
- Handle all the unplanned situations when executing the BC plan
- Promote and coordinate training activities around BC topic

In case of crisis the goal of the CMC is to mitigate the impact and restore the normal operating conditions.
