# Training

> *"The only thing worse than training your employees and having them leave is not training them and having them stay."*
> 
> **Henry Ford**

In AgileLab we believe that people's continuous learning and improvement is the only key to success.

## Training as benefit
For this reason, several [Welfare & Benefits](WelfareAndBenefits.md) are provided in order to incentive as much as possible the personal and professional growth. Specifically, you can find more info about your [annual training budget benefit](WelfareAndBenefits.md#training--personal-budget-and-training-initiatives): make good use of it!

About this personal training budget, here are two accountabilities that aren't with the `circle:Training` circle:
- Defining purchase process, meaning how to spend company money. This means payment methods with training personal budget (eg reimbursement, direct billing to Agile Lab) are defined by the `circle:Finance` circle. You can find this info on handbook on [SpendingCompanyMoney](SpendingCompanyMoney.md) page.
- Making reimbursements happen - any decision about deadlines to file expenses on ElapseIt is within the accountability of `circle:People` Circle. You can find general info on handbook on [SpendingCompanyMoney](SpendingCompanyMoney.md) page and in `circle:People` Circle communications.

Continue reading to find out how `circle:Training` can help you.

## The Training circle
AgileLab has a specific `circle:Training` to fulfill the purpose of `never stop growing`. The circle promotes upskilling as individual and collective practice. Upskilling means to improve both hard (e.g. technical) and soft skills (e.g. company values, leadership), and isn't limited to technical roles.

You are encouraged to get to know the `circle:Training` circle and its offerings in terms of accountability and roles on holaspirit.


## The portal
AgileLab has a [portal](TrainingPortal.md) that centralizes all resources regarding Training, from tutorials, to coding repositories and the courses. You can check it out at https://agilefactory.gitlab.io/training/training-portal/

### Courses
AgileLab provides internal courses in its Learning Management System (LMS). For further details, please read the [Courses](Courses.md) page.

### Workshops
AgileLab frequently organizes internal tech workshops. For further details, please read the dedicated [section](Workshops.md).

### Language Learning
AgileLab provides the possibility to improve the language skills. For further details, please read the dedicated [Language Learning](LanguageLearning.md) page.

### Other Training material
You can find plenty of other training resources:
- Slides and past training material in `sharepoint://BigData/Documents/Training/`
- books: we have a [Library](Library.md) and a [Book club](BookClub.md)

### Time Tracking

All the guidelines to be followed for time tracking on Elapseit are available in [Project Time Tracking](ProjectTimesheet.md) (focus on the Training section).
