At Agile Lab, we are committed to putting people first. When they are in the best mental and physical condition, our employees are happiest, and we believe that a good work-life balance is key to achieving this.  
Therefore, although all contracts are completely remote, we believe in the value of having physical offices, where we encourage social relationships, sharing of cultural values and professional collaboration.
This also offers great opportunities to introduce new people to the corporate culture: remember that our junior colleagues can learn from senior ones to establish attitudes and behaviours that can be useful both inside and outside the office.
This produces value for people and people produce value for the company.
At Agile Lab, therefore, all offices remain open and available to anyone who chooses to work from the office rather than from home. The choice is completely up to you. There is no need to inform yourself if you want to stay at home one day or want to meet other colleagues in the office: just follow the [operating guidelines](https://agilelab.sharepoint.com/:b:/s/bigdata/EUi28qjeh0dHm1FuMgE2CGkBZxWGXzBzIMskyvwNq7Jkaw?e=pRQq4d) for accessing the offices and mark the day of your presence on Elpaseit with the *OP - Office Presence* specification for the office you are attending.

# Offices Location

Agile Lab covers six physical locations:
* [Bologna](Bologna.md)
* [Catania](Catania.md)
* [Milano](Milano.md)
* [Padova](Padova.md)
* [Rende](Rende.md)
* [Torino](Torino.md)

# Guidelines for opening and closing an office

If the objective of Agile Lab offices is to provide employees with a space for collaboration, socialisation and growth, then an Agile Lab office only makes sense if there are at least a minimum of 5 employees residing in the same territorial area and interested in using the office.  
In this case, a contact person from this group of employees interested in opening a new office can write to the Lead Link of the People Care circle submitting their proposal, including the city in which they would like to open a new office, the number of people interested, a coworking proposal or rental solutions (if already searched for in the area concerned).
People Care will take charge of the request, evaluate the conditions for opening the new office and the solutions proposed by the employees (and/or look for new solutions), and will evaluate the possibilities and timeframe for opening the new office according to the budget available.

Otherwise, when the situation arises where an active office is little used and/or there are fewer than 5 employees living in the territorial area where the office is located, People Care and the {% role %}People Care/Facility{% endrole %} may decide to close the office.

# To start going to an office 

If you are interested in or need to start going to an Agile Lab office, the first thing you need to do is to contact the {% role %}People Care/Facility{% endrole %} of the office you are interested in. He/she will take care of adding you to the office's Teams channel and mailing list so that you can be informed about the organisation of the office, socialising events organised by colleagues who frequent the same office and all compliance aspects related to the office.
In addition, follow the operating guidelines below for requesting access to the office and check the Handbook page of the office of interest to access the guidelines on emergency procedures.

# Operating guidelines for office access control 

The [Access Control Operating Guidelines](https://agilelab.sharepoint.com/:b:/s/bigdata/EUi28qjeh0dHm1FuMgE2CGkBZxWGXzBzIMskyvwNq7Jkaw?e=pRQq4d) aim to prevent unauthorised access that could compromise the confidentiality, integrity and availability of resources and/or the smooth running of work activities.  

Agile Lab’s offices can be accessed by:
- Employees who freely decide to attend one of Agile Lab's operating sites on a regular basis, identifying their site of interest on the basis of their territorial area of residence.
- Employees coming from other territorial areas of residence, who have identified a different Agile Lab operating site as their site of interest and/or usually work full-remote.
- Long-term external consultants/collaborators and employees of external companies (e.g. maintenance, cleaning, etc.)
- Guests
- Minor
- Authorities and FF.O

## Employees and Long-term external consultants access

In the case of employees and long-term external consultant, the access control system provides for the issuing of a badge or other control device authorising entry for the period strictly necessary for the performance of their duties at the office.  

Employee badge request:
Employees who freely decide to attend one of Agile Lab's offices more or less constantly, identifying their location of interest on the basis of their territorial area of residence, must request their badge by filling out and signing the form [AGILE LAB_All.1 Office Access badge request form for employees](https://agilelab.sharepoint.com/:b:/s/bigdata/ESiDDedl_CFFpQ67ZMmf95gBzqL5b7vm7ue0oXgOmSHcow?e=YmPuWZ) and sending it via email to the {% role %}People Care/Facility{% endrole %} of the location of interest. 
The {% role %}People Care/Facility{% endrole %} will activate the badge for the employee and compile the census file. 
Employees who need access to one of Agile Lab's offices for one day only must instead follow the procedure for Guests.

External consultant badge request:
The Lead Link of the circle who needs to have consultants or external staff access the Agile Lab offices on an ongoing basis must request a consultant badge by filling out and signing the form [AGILE LAB_All.2 Office access badge request form for external collaborators](https://agilelab.sharepoint.com/:b:/s/bigdata/Ebwf6cI5oiNOpGfN_kR1H60B5bmb34EibKiaLW296AhIIQ?e=qIEeMS) and emailing it to the {% role %}People Care/Facility{% endrole %} of the office of interest. 
The {% role %}People Care/Facility{% endrole %} will activate the badge for the employee and compile the census file.

In both cases, the badge or other control device at the end of the period of collaboration must be returned and disabled. The badge holder should then contact the {% role %}People Care/Facility{% endrole %} of the office he/she attended and for which he/she was assigned a badge to organise the return and/or disabling of the badge.

In the event of anomalies or malfunctioning of the badge during access, immediate notification to the {% role %}People Care/Facility{% endrole %} by email will suffice, specifying what has occurred. 
In order to issue a new badge, a new request must be made by the employee, enclosing a copy of the request form.

In the event of badge loss or theft, if the badge does not contain any identification and/or company data, immediate notification to the {% role %}People Care/Facility{% endrole %} by email will suffice, specifying the incident. 
In the case of a badge containing identification and/or company data, it will also be necessary to file a complaint with the competent authorities (Police or Carabinieri).  
In order to issue a new badge, a new request will have to be made by the employee, attaching a copy of the request form (and of the complaint, where applicable).

## Guest Access

On the other hand, guests or other occasional accesses do not require a badge or other control device.  
The employee receiving the visit must request the guest's access by email to the Facility Manager of the site of interest and must ask the guest to complete and sign the form [AGILE LAB_All.3 Guest registration](https://agilelab.sharepoint.com/:b:/s/bigdata/EUf2OD4mOYJHvSw7nkZnSx8B4ZeqLdS8rBziMbi-q9TCkg?e=M1jCDa) and give the guest the Guest Card made available at the site of interest.  
The form must then be emailed by the employee who received the visit to the {% role %}People Care/Facility{% endrole %} of the site of interest for appropriate archiving.

## Minor Access

Minors can access, on an occasional basis, Agile Lab's operational sites only if accompanied by a parent or other adult acting on their behalf (guest or employee), subject to assuming the relevant responsibility, as provided for in the access control procedure of the individual site. The minor's access to the site is subject to the completion and delivery of the attached [AGILE LAB_All.4 Minor access form](https://agilelab.sharepoint.com/:b:/s/bigdata/EUf2OD4mOYJHvSw7nkZnSx8B4ZeqLdS8rBziMbi-q9TCkg?e=2oyjcp), to be sent by email to the {% role %}People Care/Facility{% endrole %} of the site of interest.

## FF.O. or External Authorities access for inspection visits  

It is regulated the possible access to Agile Lab's operating sites of different Authorities, such as Supervisory and Control Authorities, FF.O. Staff, AGCOM Inspectors, Government Officials, etc.  
In all these cases, the People Care Circle Contact Person or the persons who may be present at the site must immediately inform the Compliance Circle and proceed to identify the official by means of a valid personal identity document and identification of the body to which he or she belongs.  

In the course of the visit/inspection, the maximum cooperation must be provided by facilitating the access activities, taking note of the requests, keeping a copy of any documentation produced and taking care to forward it to the Compliance Circle.  

The judicial authorities and the F.F.O., as part of their investigative activities, may request access to the images recorded by the video surveillance system, which may be present in the building. In these cases, the person making the identification must acquire the documentation produced by the authorities and direct the request to the Compliance Circle, who will take care of the subsequent steps with the person who manages the maintenance of the installed system.  

## Access with company or private vehicles  

All the staff may enter Agile Lab's operating sites via the pedestrian accesses. Entry by vehicle into the internal car park of the operational site is only permitted to authorised personnel in compliance with the procedure drawn up by the territorially competent function.

In this context, access to the car park inside the operational site is permitted to:  
- Company vehicles: company vehicles may access the internal areas if the driver is authorised to do so by the {% role %}People Care/Facility{% endrole %} of the relevant operational site.  
- Employee-owned vehicles: only vehicles authorised in advance for the designated parking areas may access the internal parking areas. It is generally not permitted to leave the vehicle parked overnight, except for service requirements which must be communicated well in advance to the {% role %}People Care/Facility{% endrole %} of the operating site of interest.
- Vehicles of third party guests: where provided for, access of third party guests' vehicles to the internal car parks is subject to the availability of free spaces at the time of entry. In any case, it is only permitted from the gates designated for this service and subject to authorisation by the {% role %}People Care/Facility{% endrole %} of the operating site concerned.  
- Vehicles/vehicles of external companies carrying out third-party services: the external company, authorised in advance for access, must submit, through the Circle concerned, to the {% role %}People Care/Facility{% endrole %} of the operating site of interest, the documentation envisaged in the access control procedure. It is in any case necessary that the vehicle (model, number plate or, alternatively for work vehicles, registration number, chassis number, etc.) be communicated in advance to the guard station, where present.  

All non-company vehicles authorised to park inside the company car park are liable for damage caused to parked vehicles regardless of the provisions of their insurance policies, which must in any case be inspected by the personnel in charge on request.
