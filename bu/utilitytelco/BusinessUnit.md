# Utility & Telco

## Resource Management
There are several objectives related to resource management:

* Optimizing the BU capacity across all projects.
  * Minimizing unassigned members
  * Maximize members productivity  
    We care about people and we should avoid people working in 10 projects at the same time to fullfill the whole possible capacity.
* Monitoring business performances of the Business Unit.
  * Efficiency
  * Gross Margin

### Demand & Capacity Planning
- `Utility / General / BU People.xsls#CapacityPlan` contains the capacity plan of the BU 

### Project Portfolio Management

There are some activities related to resource management:
* Selling a project
* Starting a project
* Closing a project
* Optimize project allocation
* Customer caring and planning

#### Project Portfolio Management Concepts
- A project requires a team assigned to it. Each project has a corresponding Account in the circle Sales.
- Members assigned to a project determine the project allocation.
- A project may require a number of resources greater than the team members assigned to a project.
- The requested number of people determines the project demand.
- A project may require different roles, each role can be associated to a specific rate.
- The association of roles to rates is up to the Account.

#### Selling a project

A new project requires alignment between Sales, Engine and the BU to get the best fit for the following constraints:
- Roles and rates
- Available capacity

This may require:
* Hiring new members in time to start
* Reducing members allocated to other projects and assign them to the new one
* Dropping the project if there is not a good fit

#### Start a project

After selling projects, a BU Lead Link must setup the project:
* Assigning members to the project
* Assign a project lead
* Assign an architect
* Coach and follow the team until they get right

#### Closing a project

Closing a project requires multiple actions:
- Relocation of team members to other projects. Planning in advance would reduce the unassigned capacity of the Business Unit.
- Collecting materials and make documentation available and reusable for the company.
  - Estimation and planning: useful for future estimations
  - Architecture and ADRs
  - Key developments

#### Optimize project allocation

Sometime it is clever to move team members around projects to find a better balance between skills, productivity and serenity while keeping always in mind also customers' needs.
People fragmented over multiple projects would reduce productivity and allegedly increase stress.
Another example may be the case of members allocated on repetitive activities. This may result stressful with the risk to lose productivity and engagement by the members of the BU.
Nevertheless, everyone must be proactive and productive making their best for Agile Lab to be succesful.
Repetitive tasks can be automated, bad practices and be improved, technical debt can be reduced.
We love agilers making their project a better place to work.

#### Relocation

There are some risks tied to relocating people to different projects
* Time for onboarding
* Knowledge transfer
* Customer satisfaction

This is the reason why we must reduce onboarding time optimizing documentation and adopt bou scout rules.
Customer satisfaction in important as well and we must strive to make customer confident with our moves.

## Project Management

### Team Preparation.
Team preparation depends on actual planning. This is a delicate phase that is difficult to predict exactly.
The goal of the BU Lead is to minimize downtime and limit the risks due to premature team allocation.
Indeed, it often happens that clients stall for an unspecified period in order to organize themselves internally.
In this context, the formation of the team should consider the involvement of the Engineering Director and HR in order to coordinate the procurement of resources.

### Kick Off planning
The Kick Off is normally organized by Account and BU Lead, making sure that an initial meeting is convened to discuss timing and modalities.

#### Kick off meeting
A project starts with a kick off meeting.
The purpose of the kick off meeting is discovering reference people and coordination points.

#### Project initiation
The kick off meeting beings the initiation phase where the following information must be collected and consolidated:
- Reference people: identify key people for the project;
- Administration and billing: identify reference people for administration and billing
- Change management: identify a process to formally manage change requests
  - Inquire
  - Approval and approvers
  - Document templates
  - Internal tracking
- Communication tools:
  - Project documentation (architecture, infrastructure, ADRs, etc.)
  - Project planning (project artifacts like planning, gantt, activities, etc.)
  - Informal communication channels (teams, chats, email, etc.)
  - Formal communication channels (email, portals, meetings, etc.)
- Project methodology:
  - Waterfall (i.e. requirement => analysis => dev => test => uat => ...)
  - Agile (Scrum, Kanban, etc.)
  - Hybrid (Waterfall planning, agile development)
- Project cerimonies
  - Progress of work meeting / sprint review (weekly, biweekly, etc.)
  - Daily meeting
  - Retrospective, Project control
- Project artifacts:
  - Gantt vs product backlog
  - User story vs requirement management

The primary goal of a project lead is to reduce uncertainty.
The project initiation starts with high uncertainty and must land with low uncertainty about the project execution.  

### Project Status Report
Project Leads and Project References are asked to provide a Project Status on a monthly basis.

This is a tool to monitor, control, coordinate, share knowledge, and lower risks:
* Issues
* Resolution of issues
* Good and bad practices
* Technical and organizational news
  can be shared within the circle providing input for continuous improvement.

- Elapseit Tasks are used to trace:
  - Milestone: deliverable recognized by customers, Account, BU Lead, Project Lead and team members
  - Change Request: Fixed Price projects require strict management of Change Requests
  - Incident: a project related event that can compromise customer satisfaction and relationship

### Project Logs
Project Leads play a central role in the circle since they convey most of the information of a Business Unit.
They follow projects and can return feedback that can be very helpful to drive business unit decisions.
A business unit may organize an easy way to share those information and use self-management to address concerns and issues.
At the same time, the circle can also link information that may be used to optimize the circle.

Here some examples:
* Exceeding resources of projects to compensate for a lack of resources of another projects
* Interruption of projects due to budget limits
* Key people of a customer's organization moving around or leaving
* Business opportunities like discussion around data-related projects, requests for POCs,etc.

Information relevance is based on urgency / importance:
- Urgency: cannot be delayed
- Importance: cannot be overlooked

Reporting follows this principle:
- *Important information* must be reported as Holaspirit tensions and planned for the next available tactical.
- *Urgent information* must be reported by email to BU Lead, Account and any internal stakeholder involved. Call BU Lead and/or Account to accelerate information delivery.

### Project Layout

Every project must have a dedicated Sharepoint Folder `SHP://Utility/PROJECTS/<PROJECT_NAME>`.
The folder layout must be structured as follows:

````asciidoc
PROJECTS/<PROJECT_NAME>
|- ARCHITECTURE
|- PLANNING
|- DOMAIN_KNOWLEDGE
|- CHANGE_REQUESTS/<ELAPSEIT_CR_ID> #Fixed Price Project Only
   |- scope.pptx 
   |- estimates.xsls
````

### Project Metrics

The Microsoft Form `Utility / BU Project Assessment` must be used to compile project-related metrics.

This form collects all the necessary measures to produce:

- Circle Metrics
  - [SPACE](https://queue.acm.org/detail.cfm?id=3454124)
  - Average of Velocity [%] (Completed backlog / Planned backlog)
  - Average of Efficiency [%] (Billed hours / Billable hours)

### People Management

#### Career Path

The career ladder is complex and requires a certain effort to manage it seriously over many members. 

Having a clear pictures of the circle’s members may help a BU Lead to keep up with their progresses.

Also, the BU Leas takes notes about each member and check if it is time to consider any actions for improvement or recognition of maturity.

#### 1o1s

1-o-1s meetings is a safe space for people to grow.

A BU Lead may find it difficult to schedule many ad-hoc meetings periodically.

It may be useful to introduce some practices in the BU circle to make the life easier for everyone.

The BU Lead proposes a fixed schedule where the circle members can participate through a planned schema.

Such a schema is very simple but still requires education to make it work well.

Expect an initial period to let anyone getting the new habit. Thus, it will go easy and well.

If the BU starts growing, the BU Lead can relax the calendar to fit everyone needs we some extra easy rules.

For instance, it is possible to split the fixed schedule between even and odd weeks and run bi-weekly meetings per member.

Do not use 1-o-1s for project alignment. There will be circle tacticals and other occasions to do that. Focus on personal and professional growth of circle’s members and issues that cannot be addressed elsewhere.

The  BU Lead wants to take notes about BU members. This should help trace short/long term objectives, personal and professional growth achievements, etc.

- `Utility / General / BU People.xsls#1o1s` contains the scheduling of 1o1s between Project Leads and BU Lead
- 1o1s are managed through Microsoft Tasks, each Project Lead shares a List with the BU Lead
- A single List contains any information or goal shared between Project Lead and BU Lead

#### Skip Level 1o1s (SL1o1s)

As a BU Lead,  it is very useful gathering feedbacks not from the front lines only.

Periodically setup a schedule up with all members of the circle.

This helps a BU Lead keeping in contact with everyone and gaining insights about the BU itself, projects, feedbacks for project leads and references, customers’s info, etc.

This 1-o-1s may require a larger timeframe since you are running those meetings with members you usually talk less.

- The BU Lead plans quarterly 1o1s with a sample of team members within BU projects.

#### 360 review
- See [360 Review](/360Review.md)

#### Salary Review
- See [Salary Review](/SalaryReview.md)

##### Principles

Two main principles to take into account while managing salaries for the members of a Business Unit
* Meritocracy
* Fairness

The career ladder provides the right perspective to assign and review salaries.
Other considerations are necessary to include factors that may not be captured by the career ladder.
Here some examples:
* Previous professional experiences significant for our business
* Changes in the job market
* Specific working conditions

##### Salary reviews

At company level, AgileLab provides an annual budget for cost growth.
This must be taken into account while reviewing salaries.

As BU Lead wants to optimize the total budget and allocate such a budget along the year.

Reminder: BU leads can review salaries of members behind of two Ladder levels.

See [Salary Review](/SalaryReview.md) for more information.

#### Coaching
- Coaching is requested on demand or sensed by the BU Lead
- Coaching is reported on `[YYYY Agile - Consulting - Coaching]` on Elapseit

##### Customer caring and planning

The BU Lead periodically checks for customer satisfaction and for opportunities to grow.

This is very useful to move the demand plan and provide Accounts (circle Sales) with inputs to sell.

This activity also reduces the risk to surprises. Sampling over time customer needs, we’ll let the BU Lead to know more about news and customer’s internal dynamics.

Also, this is an occasion for the BU Lead to build her/his knowledge about the customer’s organization (that is often complex and unclear)

The Microsoft Form `Utility / BU Customer Satisfaction` is used to record customer satisfaction notes with the customer.

## Bid Management

Participation in the tender is instructed by [Successful Bid](/InternalSales.md#bid-management) according to the following accountabilities:
- Tender Officer: Administrative Response.
- BU Lead: technical proposal including, delivery, services, products, BU Leads can involve internal resources to address specific competencies to prepare the best answer for the Bid.
- Account: Financial Proposal.

### Technical and economic bid

The format of the technical and economic bid depends on the client and the subject matter.
Close cooperation between Tender Officer, BU Lead and Account is always necessary so that the data provided, 
the form and structure of the content, and the economic bid comply with the specifications of the tender and do not violate any constraints for participation in the tender. 
In addition, Account and BU Lead must coordinate all necessary bid evaluation activities by service and product partners for the purpose of formulating the best bid.

### Tender outcome notification

To facilitate conversations in the management of tenders, we make use of the Utility Internal Sales channel (or channels specifically created for the management of a specific tender).
These channels are used to communicate the outcome of important events with respect to the execution of the tender, as well as to organize any KickOff preparations for project activities.

## Communication & Knowledge Sharing

### Recurring Business Unit Newsletter
The Business Unit sends a quarterly newsletter to share results with the rest of the company.

This is managed by the role `Generator`.

### Thought Leadership

The Business Unit makes a collective effort to expose results and contribute to Agile Lab's thought leadership through articles, and public speaking.

### Utility & Telco Lab

The Business Unit has a weekly internal knowledge sharing meeting to discuss about issues and solutions of general interest for our project teams.

### Utility Domain Knowledge

Project Leads must organize domain knowledge within `Utility / Domain Knowledge / Utility Domain Knowledge.pptx`
This documentation is oriented to the data domain and business cases. It is less technical and more business.

`Utility / Domain Knowledge / Organization` contains a presentation of the organization of our customers.

This documentation is necessary for multiple needs:
- Project Onboarding
- Sales and Presales activities
- Knowledge Sharing

## Circle Metrics
A Business Unit has to be monitored through different perspectives.

The following metrics are currently used to trace the productivity of the Business Unit:
* Efficiency
* Gross Margin

Software quality metrics and indicators are computed from project metrics to align with Consulting:
  - Average of Defects Registered In Backlog
  - Average of Code Coverage [%]
  - Average of Perceived Technical Debt [Mandays]
  - Average of SW
  - Average of DE
  - Average of DS
and are placed in `Consulting / metrics / consulting metrics YYYY`.